# Reto BackEnd
Crear un Servicio RESTFUL que consistirá en poder crear, editar(actualizar), eliminar una institución, pero para eliminar se debe validar que dicha institución no esté siendo usada por una carrera; la institución puede tener una a muchas carreras y una carrera pertenece a una o muchas instituciones. Adicionalmente, el servicio debe permitir crear. editar(actualizar) o eliminar una carrera.

Requesitos necesarios:
  - Conocimientos JavaScript ES6
  - Conociemientos de AdonisJS
  - Conocimientos de Rutas, migraciones, controladores, validaciones, migrations
  - Relaciones entre modelos.

Finalidad del reto, es obtener lo siguiente: :eyes: :eyes:
  - Comprobar conocimientos de JavaScript.
  - Legibilidad de código.
  - Aplicación de conceptos AdonisJS.
  - Uso de Git.
  - Realizar Merge Request(GitLab).

Para el reto se plantearan pasos a seguir, no necesariamente estos pasos se deben seguir al pie de la letra.

1. Tener una cuenta en Gitlab.

2. Realizar Fork al repositorio base que contiene el código AdonisJS.

3. Preparar su ambiente para realizar los Merge Request.

Una vez haya hecho los pasos anteriores con respecto a Git, nos ponemos manos a la obra. :v: :punch:

4. Inicializar el proyecto. En caso de no tener conocimientos sólidos de AdonisJS leer su documentación ([Adonis Guide](https://adonisjs.com/docs/4.1/installation)).

5. Puedes usar las siguientes bases de datos(mysql, sqlite o Postgres) ya sea de manera local o remoto. Elegir el de su preferencia.
  
Acá empieza lo divertido. :muscle: :metal:

6. Crear las migraciones de las tablas y sus columnas. Imagen "**MER.png**". ([Migrations Adonis Guide](https://adonisjs.com/docs/4.1/migrations#sponsor))

7. Generar datos de pruebas, para ello puedes usar seeders, faker o realizar una combinación de ambos. Igual se evalúa el método a usar.
    1. Los datos en duro para la tabla **Tipo Institución** son los siguientes:
        1. Universidad
        2. Instituto

Perfecto! Si llegaste a hasta aquí podrás visualizar **DATOS FICTICIOS** en la base de datos que estás usando como tambien sus relaciones correspondientes Continuemos con la diversión. :muscle: :metal:

8. Ahora, tienes que crear los modelos y las relaciones correspondientes a las tablas creadas previamente.

9. Crear los controladores y sus funciones correspondientes.

10. Crear las siguientes validaciones.

* Tabla Instituciones:
    1. La columna nombre sólo acepta texto. Validar que no se ingrese números ni caracter especial.
    2. La columna teléfono sólo debe permitir ingresar 9 números.
    3. La columna email sólo debe permitir ingresar en formato correo electrónico: miemail@reto.com.pe
    4. La columna idTipo sólo permite números.
* Tabla Carreras:
    1. La columna nombre sólo acepta texto. Validar que no se ingrese números ni caracter especial.

* La tabla Tipo Insitución no tendrá validaciones ya que no cuenta con un CRUD, además, sólo se llenan con datos en duro.

11. Si todo esta bien, prueba tu servicio sin miedo. :muscle:

Se tomará en cuenta la forma en que plantea la solución, la forma de codificación, la aplicación de conceptos de AdonisJS. No tengas miedo a equivocarte, de las equivocaciones se aprende. Consulta si hay algo que no te queda claro en el reto. :hand: :hand:

Finalmente, se terminará el reto realizando un Merge Request usando la platafoma GitLab.

# :muscle:¡Suerte!:metal: